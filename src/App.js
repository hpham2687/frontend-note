import React from 'react';
import './App.css';
import Header from './components/Header'
import BodyManager from './containers/BodyManager'
import KeyWordManager from './containers/KeyWordManager'

import { BrowserRouter as Router, Route, Link, NavLink } from "react-router-dom";
function App() {
    
    return ( <>
    	<Router>
        <Header/>

            <Link to='/manager'> click herre</Link>
            <br/><br/>    
            <Route exact path="/manager/keyword" component={KeyWordManager} />

            <Route exact path="/manager" component={BodyManager} />
        </Router>
        </>
    );
}

export default App;