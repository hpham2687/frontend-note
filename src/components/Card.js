import React from 'react';


class Card extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {

        return ( <>
            <div className="card">
        <div className="card-header">
          <a className="card-link collapsed" data-toggle="collapse" href={`#${this.props.noteTitle}`} aria-expanded="false" aria-controls={this.props.noteTitle} >{this.props.noteTitle} </a>
         <a className="card-link" onClick={() => this.props.clickEditCards()} className="edit-card">Edit</a>
        </div>
        <div id={ this.props.noteTitle}  className="collapse">
          <div className="card-body">
      {this.props.noteContent}
          </div>
        </div>
      </div>

            </>
        );
    }
}
export default Card;