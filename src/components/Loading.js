import React, { Component } from 'react';
import loading from './../commons/assets/loading.gif'
import {connect} from 'react-redux'


class Loading extends Component {
    render() {
        let {isShowLoading} = this.props;
        let html = null;
        if (isShowLoading)
            html = (
                <div className="global-loading">
                <img className="loading-img" src={loading} alt="loading"/>
            </div>
            );
        return html;
    }
}


const mapStateToProps = (state) => {
    return {
        isShowLoading: state.loading.isShowLoading,
    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        
        
    } 
}


export default connect(mapStateToProps, mapDispatchToProps)(Loading);
