import React from 'react';


class Card extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            //mode: null
        }

    }
    renderEditMode() {
        var { dataEditingNote } = {...this.props};
        return ( <>
            <form>
  <div className="form-group">
    <label>Title</label>
    <input type="text" className="form-control" id="exampleFormControlInput1" value={dataEditingNote.title}/>
  </div>
 
  <div className="form-group">
    <label>Keyword</label>
    <select multiple className="form-control" id="exampleFormControlSelect2">
      <option>1</option>
      <option>2</option>
      <option>3</option>
      <option>4</option>
      <option>5</option>
    </select>
  </div>
  <div className="form-group">
    <label>Content</label>
    <textarea className="form-control" id="exampleFormControlTextarea1" rows="3" value={dataEditingNote.content}></textarea>
  </div>
    <button type="button" className="btn btn-outline-primary">Save</button>
        <button onClick={()=>{
        this.props.changeMode()
        }} type="button" className="btn btn-outline-primary">Close</button>


</form>

            </>
        );
    }
    renderAddMode() {
        return ( <>
            <form>
  <div className="form-group">
    <label>Title</label>
    <input type="text" className="form-control" value="" placeholder="hoc react"/>
  </div>
 
  <div className="form-group">
    <label>Keyword</label>
    <select multiple className="form-control">
      <option>1</option>
      <option>2</option>
      <option>3</option>
      <option>4</option>
      <option>5</option>
    </select>
  </div>
  <div className="form-group">
    <label>Content</label>
    <textarea className="form-control"  value=""  rows="3"></textarea>
  </div>
    <button type="button" className="btn btn-outline-primary">Add Note</button>

</form>

            </>
        );
    }
    render() {
        if (this.props.mode == "ADD_MODE")
            return this.renderAddMode();
        else if (this.props.mode == "EDIT_MODE") {
            return this.renderEditMode();


        }
    }
}
export default Card;