import * as env from './../env'
export const showLoading = () => ({
    type: env.SHOW_LOADING
})
export const hideLoading = () => ({
    type: env.HIDE_LOADING
})