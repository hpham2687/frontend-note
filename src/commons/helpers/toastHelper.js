import { toast } from 'react-toastify';


export const notifySuccess = (mess) => toast(`🦄 Wow so easy! ${mess}`, {
    position: "top-left",
    autoClose: 5000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
    });