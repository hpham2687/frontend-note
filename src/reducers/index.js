import {combineReducers} from 'redux'
import noteReducer from './noteReducer'
import loadingReducer from './loadingReducer'
export default combineReducers({
    notes: noteReducer,
    loading: loadingReducer,
})