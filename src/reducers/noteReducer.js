import * as env from '../env'
const DEFAULT_STATE = {
    listNotes: [],
    listKeywords: [],
    dataFetched: false,
    isFetching: false,
    error: false,
    errorMessage: null
}
export default (state = DEFAULT_STATE, action) => {
    switch (action.type) {
        case env.GET_ITEM_REQUEST:
            return {
                ...state,
                isFetching: true
            }
        case env.GET_ITEM_SUCCESS:
            return {
                ...state,
                isFetching: false,
                dataFetched: true,
                error: false,
                errorMessage: null,
                listNotes: action.payload
            }    
        case env.GET_ITEM_FAILURE:
            return {
                ...state,
                isFetching: false,
                error: true,
                errorMessage: action.payload.errorMessage
            }   
        case env.GET_KEYWORD_REQUEST:
            return {
                ...state,
                isFetching: true
            }
        case env.GET_KEYWORD_SUCCESS:
            return {
                ...state,
                isFetching: false,
                dataFetched: true,
                error: false,
                errorMessage: null,
                listKeywords: action.payload
            }    
        case env.GET_KEYWORD_FAILURE:
            return {
                ...state,
                isFetching: false,
                error: true,
                errorMessage: action.payload.errorMessage
            }               
        default:
            return state;
    }
}