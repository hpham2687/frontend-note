import * as env from '../env'

const DEFAULT_STATE = {
    isShowLoading: false
}

export default (state = DEFAULT_STATE, action) => {
    switch (action.type) {
        case env.SHOW_LOADING:
            return {
                ...state,
                isShowLoading: true
            }
        case env.HIDE_LOADING:
            return {
                ...state,
                isShowLoading: false
            }
                        

        default:
            return state;
    }

}