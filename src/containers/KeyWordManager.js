import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as env from '../env'

class KeyWordManager extends Component {
    componentDidMount() {
        console.log("here1");
       // await this.getAllNotes();

        this.props.initialLoad()
        // console.log(response);
        // console.log(this.state.cards);

    }
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-8">
                                keyword manager
                    </div>
                </div>
            </div>

        );
    }
}
const mapStateToProps = (state) => {
    return {
        notes: state.notes.listNotes,
        isFetching: state.notes.isFetching

    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        initialLoad: (payload) => {
            dispatch({type: env.GET_KEYWORD_REQUEST, payload})
        }
    } 
}
export default connect(mapStateToProps, mapDispatchToProps)(KeyWordManager);
