import React from 'react';
import ActionTab from '../components/ActionTab'
import Card from '../components/Card'
import NoteApi from '../api/noteApi'
import * as env from '../env'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'; // ES6

const axios = require('axios');

 
class BodyManager extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cards: null,
            wordFilter: '',
            mode: "ADD_MODE",
            dataEditingNote: null
        }
    }
    componentDidMount() {
        console.log("here1");
       // await this.getAllNotes();

        this.props.initialLoad()
        // console.log(response);
        // console.log(this.state.cards);

    }
    clickEditCard(id, title, content) {
        var dataEditingNote = {
            id: id,
            title: title,
            content: content
        }
        this.setState({ dataEditingNote: dataEditingNote, mode: "EDIT_MODE" })

    }
    handleChangeSearch(e){
        console.log(e.target.value)
        this.setState({wordFilter: e.target.value})
    }
    renderAllNotes() {
        const { notes } = this.props;
        console.log(this.props.notes);
        console.log('abdd')

        const allNotes = this.props.notes.map((aNote, num) => {        
            var n = aNote.noteTitle.includes(this.state.wordFilter);
            if (n)
            return <Card clickEditCards = {() => this.clickEditCard(aNote._id, aNote.noteTitle, aNote.noteContent) } key={num} id={aNote._id} noteTitle={aNote.noteTitle} noteContent={aNote.noteContent}/>

        });
        return allNotes;
    }
    handleChangeMode(){
     //   alert('click');
        this.setState({dataEditingNote:null, mode: "ADD_MODE" })
       // console.log('dome')
    }
    render() {
        console.log('render')     
            return ( <>
                <div className="container">

  <div className="row">
    <div className="col-8">
             <input className="form-control" type="search" onChange={(e) => this.handleChangeSearch(e)} placeholder="Search" aria-label="Search" />
<br/>
      {this.renderAllNotes()}
    </div>
   
    <div className="col">
     <ActionTab dataEditingNote={this.state.dataEditingNote} changeMode={() => this.handleChangeMode()} mode={this.state.mode}/>
    </div>
  </div>
    </div>


                </>
            );
    }
}
BodyManager.propTypes = {
    notes: PropTypes.array,
    isFetching: PropTypes.bool,
    initialLoad: PropTypes.func
}
BodyManager.defaultProps = {
    notes: [],
    isFetching: false
  };

const mapStateToProps = (state) => {
    return {
        notes: state.notes.listNotes,
        isFetching: state.notes.isFetching

    }
}
const mapDispatchToProps = (dispatch) => {
    return{
        initialLoad: (payload) => {
            dispatch({type: env.GET_ITEM_REQUEST, payload})
        }
    } 
}
export default connect(mapStateToProps, mapDispatchToProps)(BodyManager);