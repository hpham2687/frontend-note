import {put, takeEvery, delay} from 'redux-saga/effects'
import NoteApi from '../api/noteApi'
import * as env from '../env'
import * as notify from '../commons/helpers/toastHelper'
import * as loadingAction from '../actions/loadingAction'
function *getListNotes(action){
    try{
        yield put(loadingAction.showLoading())
        const res = yield NoteApi.getAllNotes({});        
        yield put({
            type: env.GET_ITEM_SUCCESS,
            payload: res
        })
        delay(1000)
        notify.notifySuccess("scuess");

    }catch(err){
        console.log('err'+err)
        yield put({
            type: env.GET_ITEM_FAILURE,
            payload: err
        })
        notify.notifySuccess(err);

    }
    yield put(loadingAction.hideLoading())

}

function *getListKeywords(action){
    try{
        yield put(loadingAction.showLoading())
        const res = yield NoteApi.getAllKeywords({});        
        yield put({
            type: env.GET_KEYWORD_SUCCESS,
            payload: res
        })
        delay(1000)
        notify.notifySuccess("scuess");

    }catch(err){
        console.log('err'+err)
        yield put({
            type: env.GET_KEYWORD_FAILURE,
            payload: err
        })
        notify.notifySuccess(err);

    }
    yield put(loadingAction.hideLoading())

}
export const noteSaga = [
    takeEvery(env.GET_ITEM_REQUEST, getListNotes)
]

export const keywordSaga = [
    takeEvery(env.GET_KEYWORD_REQUEST, getListKeywords)
]


 