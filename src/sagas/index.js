import {all} from 'redux-saga/effects'
import {noteSaga, keywordSaga} from './noteSaga'

function *rootSaga(){
    yield all([
        ...noteSaga, ...keywordSaga])
}
export default rootSaga;